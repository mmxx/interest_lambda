from .settings import INTEREST_RATE_PERCENTAGE_ANUALLY, WORK_DAYS_YEAR

INTEREST_RATE_ANUALLY = INTEREST_RATE_PERCENTAGE_ANUALLY / 100


def _interest(amount_in_cents, new_deposit_per_week_in_cents, qty_weeks):
    """
    This function is the core of the calculation.
    We use the formula "M = P . (1 + i) ^ t/252" and recursion for
    each week of deposit.
    It assumes that all values are in cents.
    """

    # Execute the formula of interest calculation
    current_amount = amount_in_cents * (
        (1 + INTEREST_RATE_ANUALLY) ** (5 / WORK_DAYS_YEAR)
    )

    # if it's the last week, we return the current amount
    if qty_weeks == 1:
        return current_amount

    # if it's not the last week, we call the function recursively minus one week
    return _interest(
        current_amount + new_deposit_per_week_in_cents,
        new_deposit_per_week_in_cents,
        qty_weeks - 1,
    )


def calc_interest(initial_amount, new_deposit_per_week, qty_weeks):
    """
    This function receives the data and calls the _interest function calculation,
    returning the result in R$ and rounded.

    Args:
      - initial_amount: The initial deposit in R$
      - new_deposit_per_week: Deposit per week in R$.
      - qty_weeks: How many weeks will occurs the deposits.
    """

    # Convert the values to cents for a better calculation
    amount_in_cents = initial_amount * 100
    new_deposit_per_week_in_cents = new_deposit_per_week * 100

    # Call the function that will work the calculation
    result_amount_in_cents = _interest(
        amount_in_cents, new_deposit_per_week_in_cents, qty_weeks
    )

    # Convert back the values for R$ and return the result
    result_amount = result_amount_in_cents / 100
    return "R$ {}".format(round(result_amount, 2))
