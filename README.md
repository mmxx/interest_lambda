## Interest Lambda

An AWS Lambda implementation of [interest](https://bitbucket.org/mmxx/interest/src/master/).

This implementations was built using [Chalice](https://github.com/aws/chalice).


A live version of the project: https://ay1eizult8.execute-api.us-east-1.amazonaws.com/api/?initial=100&investment=100&weeks=36

# How to deploy

1 - Clone the repo

2 - Create a new virtualenv(at least python3.6)

3 - Install the requirements using pip: `pip install -r requirements.txt`

4 - Execute the command `chalice deploy`

You must have your security credentials of AWS condifured on your computer. You can follow [this tutorial](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html) to have it working.
