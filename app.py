from chalice import BadRequestError, Chalice

from chalicelib.interest_calculation import calc_interest

app = Chalice(app_name="interest")
app.debug = True


@app.route("/", methods=["GET"])
def index():
    if not app.current_request.query_params:
        raise BadRequestError("params missing")

    initial_amount = float(app.current_request.query_params.get("initial") or 0)
    investment = float(app.current_request.query_params.get("investment") or 0)
    weeks = int(app.current_request.query_params.get("weeks") or 1)

    if weeks == 0:
        raise BadRequestError("weeks must be at least 1")

    print(calc_interest(initial_amount, investment, weeks))
    return {"final_amount": calc_interest(initial_amount, investment, weeks)}
